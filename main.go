package main

import (
	"bufio"
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"sync"

	"gitlab.com/eemj/proxy-checker/internal/checker"
	"gitlab.com/eemj/proxy-checker/internal/domain"
	"gitlab.com/eemj/proxy-checker/internal/publicip"
)

const (
	maxThreads int    = 250
	requestUri string = "https://v4.ident.me"
)

func main() {
	if len(os.Args) < 2 {
		log.Fatal("missing proxy list file")
	}

	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt, os.Kill)
	defer cancel()

	proxyList := os.Args[1]

	fs, err := os.Open(proxyList)
	if err != nil {
		log.Fatal(err)
	}

	defer fs.Close()

	f, err := os.OpenFile("tested_"+proxyList, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0o600)
	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	scanner := bufio.NewScanner(fs)

	var wg sync.WaitGroup
	concurrent := make(chan struct{}, maxThreads)
	proxies := make(chan *domain.ProxyCheck)

	publicipSvc := publicip.NewService()
	checkerSvc := checker.NewService(publicipSvc)

	for scanner.Scan() {
		wg.Add(1)
		concurrent <- struct{}{}
		go func(proxy string) {
			defer func() {
				<-concurrent
				wg.Done()
			}()

			proxyCheck, err := checkerSvc.Check(ctx, proxy)
			if err == nil {
				proxies <- proxyCheck
			}
		}(scanner.Text())
	}

	if err = scanner.Err(); err != nil {
		log.Fatal(err)
	}

	go func() {
		valid := 0
		for proxy := range proxies {
			if proxy.Leak() {
				log.Printf("'%s' proxy would have leaked our address", proxy.Proxy)
				continue
			}

			if _, err := fmt.Fprintf(f, "%s\n", proxy.Proxy); err != nil {
				log.Fatal(err)
			}

			valid++
			log.Printf("(%d) %s", valid, proxy.Proxy)
		}
	}()

	wg.Wait()
}

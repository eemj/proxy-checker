package publicip

import (
	"context"
	"io"
	"net/http"
	"net/netip"
)

const (
	v4URL = "https://v4.ident.me"
	v6URL = "https://v6.ident.me"
)

type Service interface {
	GetV4(ctx context.Context) (ip netip.Addr, err error)
}

type service struct{}

// GetV4 implements Service
func (s *service) GetV4(ctx context.Context) (ip netip.Addr, err error) {
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, v4URL, nil)
	if err != nil {
		return ip, err
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return ip, err
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return ip, err
	}

	return netip.ParseAddr(string(body))
}

func NewService() Service {
	return &service{}
}

package domain

import "net/netip"

type ProxyCheck struct {
	OriginalAddr netip.Addr
	ReplyAddr    netip.Addr
	Proxy        netip.AddrPort
}

func (p ProxyCheck) Leak() bool {
	return p.OriginalAddr.Compare(p.ReplyAddr) == 0
}

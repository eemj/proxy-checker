package checker

import (
	"context"
	"errors"
	"io"
	"net/http"
	"net/netip"
	"net/url"
	"sync/atomic"
	"time"

	"gitlab.com/eemj/proxy-checker/internal/domain"
	"gitlab.com/eemj/proxy-checker/internal/publicip"
)

type Service interface {
	Check(ctx context.Context, proxy string) (check *domain.ProxyCheck, err error)
}

type service struct {
	publicipSvc publicip.Service
	publicAddr  atomic.Pointer[netip.Addr]
}

// Check implements Service
func (s *service) Check(ctx context.Context, proxy string) (check *domain.ProxyCheck, err error) {
	currentAddr := s.publicAddr.Load()
	if currentAddr == nil || !currentAddr.IsValid() {
		addr, err := s.publicipSvc.GetV4(ctx)
		if err != nil {
			return nil, err
		}
		s.publicAddr.Swap(&addr)
		currentAddr = &addr
	}

	proxyAddrPort, err := netip.ParseAddrPort(proxy)
	if err != nil {
		return nil, err
	}
	if proxyAddrPort.Addr().Is6() { // Skip IPv6
		return nil, errors.New("ipv6 not supported yet")
	}

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, "https://v4.ident.me", nil)
	if err != nil {
		return nil, err
	}

	proxyTransport := http.DefaultTransport.(*http.Transport).Clone()
	proxyTransport.Proxy = func(_ *http.Request) (u *url.URL, err error) {
		u, err = url.Parse("http://" + proxy)
		return
	}
	client := &http.Client{Timeout: 30 * time.Second, Transport: proxyTransport}

	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		return nil, errors.New("status code != 200")
	}

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	replyAddr, err := netip.ParseAddr(string(body))
	if err != nil {
		return nil, err
	}

	return &domain.ProxyCheck{
		OriginalAddr: *currentAddr,
		Proxy:        proxyAddrPort,
		ReplyAddr:    replyAddr,
	}, nil
}

func NewService(
	publicipSvc publicip.Service,
) Service {
	return &service{
		publicipSvc: publicipSvc,
		publicAddr:  atomic.Pointer[netip.Addr]{},
	}
}
